package main;

import game.Card;
import game.Game;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static Game game = new Game();

	public static void printCard(Card card) {
		int value = card.getValue();
		if (value <= 10 ) {
			System.out.println(value);
		} else {
			switch (value) {
			case 11:
				System.out.println("As");
			}
		}
	}
	
	public static void printPlayerCards(Game game) {
		System.out.println("Twoje karty:");
		for (Card card : game.getPlayer().getCards()) {
			printCard(card);
		}
	}
	
	public static boolean parseChoice(String choice) {
		switch(choice) {
		case "d":
			game.hitPlayer(1);
			System.out.print("Wylosowana karta: ");
			ArrayList<Card> cards = game.getPlayer().getCards();
			printCard(cards.get(cards.size()-1));
			break;
		case "p":
			game.onPlayerPass();
			break;
		case "q":
			return true;
		default:
			break;
		}
		return false;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		game.startGame();
		System.out.println("d - dobierz karte, p - pass, q - zakoncz program");
		System.out.println("Widoczna Karta krupiera:");
		printCard(game.getDealer().getCards().get(0));
		
		printPlayerCards(game);

		while(!game.isFinished() && !parseChoice(scanner.next())) {
			game.checkPoints();
		}
		
		
		System.out.println("Koniec gry.");
		System.out.print("Wygral: ");
		if (game.hasPlayerWon()) {
			System.out.println("gracz");
		} else {
			System.out.println("krupier");
		}
		
		System.out.println("Karty gracza:");
		for (Card card : game.getPlayer().getCards()) {
			printCard(card);
		}
		System.out.printf("Punkty: %d\n", game.getPlayer().getPoints());
		
		System.out.println("Karty krupiera:");
		for (Card card : game.getDealer().getCards()) {
			printCard(card);
		}
		System.out.printf("Punkty: %d\n", game.getDealer().getPoints());
		
		scanner.close();
	}

}
