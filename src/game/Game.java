package game;

import java.util.Random;

public class Game {
	private Player dealer = new Player();
	private Player player = new Player();
	private Random generator = new Random();
	
	boolean playerWon;
	boolean gameFinished;
	
	public boolean isFinished() {
		return gameFinished;
	}
	
	public boolean hasPlayerWon() {
		return playerWon;
	}
	
	private Card getRandomCard() {
		
		return new Card(generator.nextInt(10)+2);
	}
	
	public boolean checkPoints() {
		return checkPoints(false);
	}
	public boolean checkPoints(boolean finalCheck) {
		int playerPoints = player.getPoints();
		int dealerPoints = dealer.getPoints();
		
		if (dealerPoints > 21) {
			gameFinished = true;
			playerWon = true;
		} else if (playerPoints > 21) {
			gameFinished = true;
			playerWon = false;
		} else if (finalCheck) {
			if (dealerPoints >= playerPoints) {
				playerWon = false;
			} else {
				playerWon = true;
			}
			gameFinished = true;
		}
		return gameFinished;
	}
	
	public void onPlayerPass() {
		if (!checkPoints()) {
			int dealerPoints = dealer.getPoints();
			while (dealerPoints < 16  && !checkPoints()) {
				hitDealer(1);
				dealerPoints = dealer.getPoints();
			}
			checkPoints(true);
		}
	}
	
	public void startGame() {
		hitPlayer(2);
		hitDealer(2);
		gameFinished = false;
	}
	
	public void hitDealer(int n) {
		for (int i = 0; i < n; ++i) {
			dealer.addCard(getRandomCard());
		}
	}
	
	public void hitPlayer(int n) {
		for (int i = 0; i < n; ++i)
			player.addCard(getRandomCard());
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Player getDealer() {
		return dealer;
	}
}
