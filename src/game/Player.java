package game;

import java.util.ArrayList;

public class Player {
	private ArrayList<Card> cards = new ArrayList<Card>();
	
	public int getPoints() {
		int ret = 0;
		for (Card card : cards) {
			ret += card.getValue();
		}
		return ret;
	}
	
	public void addCard(Card card) {
		cards.add(card);
	}
	
	public ArrayList<Card> getCards() {
		return cards;
	}
}
